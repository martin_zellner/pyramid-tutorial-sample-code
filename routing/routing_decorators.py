from wsgiref.simple_server import make_server
from pyramid.config import Configurator
from pyramid.response import Response
from pyramid.view import view_config
import email.utils

@view_config(route_name='time_server')
def get_time(request): 
    return Response(body = email.utils.formatdate(localtime=True))

if __name__ == '__main__':
    config = Configurator()
    config.add_route('time_server', '/time')
    config.scan()

    app = config.make_wsgi_app()
    server = make_server('0.0.0.0', 8080, app)
    server.serve_forever() 
