from wsgiref.simple_server import make_server
from pyramid.config import Configurator
from pyramid.response import Response
from pyramid.view import view_config
import pytz
import datetime
import time

def get_time_in_timezone(region, city):
    target_zone = region.capitalize() + '/' + city.capitalize()
    time_zone = pytz.timezone(target_zone)
    target_time = datetime.datetime.now(time_zone)
    return target_time.strftime('%d-%m-%Y %H-%M-%S')


@view_config(route_name='time_server')
def get_time(request): 
    try:
        return Response(get_time_in_timezone(request.matchdict['region'], request.matchdict['city']))
    except pytz.UnknownTimeZoneError:
        return Response('Unknown Timezone')

if __name__ == '__main__':
    config = Configurator()
    config.add_route('time_server', '/time/{region}/{city}')
    config.scan()

    app = config.make_wsgi_app()
    server = make_server('0.0.0.0', 8080, app)
    server.serve_forever() 
