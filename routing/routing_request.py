from wsgiref.simple_server import make_server
from pyramid.config import Configurator
from pyramid.response import Response
from pyramid.view import view_config
import pytz
import datetime
import time

def get_timezone(region, city):
    target_zone = region.title() + '/' + city.title()
    time_zone = pytz.timezone(target_zone)
    target_time = datetime.datetime.now(time_zone)
    return target_time.strftime('%d-%m-%Y %H-%M-%S')

@view_config(route_name='time_server')
def get_time(request): 
    try:
        return Response(get_timezone(request.GET['region'], request.GET['city']))
    except pytz.UnknownTimeZoneError:
        return Response('Unknown Timezone')
    except KeyError:
        return Response('Please specify a region and a city')

if __name__ == '__main__':
    config = Configurator()
    config.add_route('time_server', '/time')
    config.scan()

    app = config.make_wsgi_app()
    server = make_server('0.0.0.0', 8080, app)
    server.serve_forever() 
