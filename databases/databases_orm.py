from wsgiref.simple_server import make_server
from pyramid.config import Configurator
from pyramid.response import Response
from pyramid.view import view_config
import pytz
import datetime
from sqlalchemy import create_engine, MetaData, Table, Column, Integer, String, update, select
from sqlalchemy.sql import and_
from sqlalchemy import orm
from sqlalchemy.orm import sessionmaker

# create dummy class for orm mapper
class timezone_class(object):
    pass

# same as above
engine = create_engine('sqlite:///clicks.db', echo=True)
meta = MetaData()
meta.bind = engine
time_zone_table = Table('time_zone', meta, autoload=True)

# map our time_zone_table to our dummy timezone_class
orm.mapper(timezone_class, time_zone_table)

# we create a sessionmaker which will be used to create new sessions
Session = sessionmaker(bind=engine, autocommit=True)


def get_time_in_timezone(timezone_tuple):
    target_zone = '/'.join([x.title() for x in timezone_tuple])
    time_zone = pytz.timezone(target_zone)
    target_time = datetime.datetime.now(time_zone)
    return target_time.strftime('%d-%m-%Y %H-%M-%S')

def update_click_counter(time_zone_name):
    # We create a session, it will start right away
    session = Session()

    # used together with autocommit=True, enables automatic commit
    with session.begin():
        # query the database by filtering the matching name and access the first element
        # we receive a complete working timezone_class object here,
        # which has the attributes id, name and clicks set
        query = session.query(timezone_class).filter(timezone_class.name == time_zone_name).first()
        
        # we increase our counter by one
        updated_clicks = query.clicks + 1
        
        # store it back in our object
        query.clicks = updated_clicks
        
        # adding our query back to our database, 
        # due to the fact that we modify our received object, 
        # the corressponding object in the database will be updated and not readded
        session.add(query)
        
        return updated_clicks

# same as above
@view_config(route_name='time_server')
def get_time(request): 
    try:
        time_in_timezone = get_time_in_timezone(request.matchdict['timezone'])
        current_clicks = update_click_counter("/".join(request.matchdict['timezone']))
        return Response(time_in_timezone + " " + str(current_clicks))
    except pytz.UnknownTimeZoneError:
        return Response('Unknown Timezone')

if __name__ == '__main__':
    config = Configurator()
    config.add_route('time_server', '/time/*timezone')
    config.scan()

    app = config.make_wsgi_app()
    server = make_server('0.0.0.0', 8080, app)
    server.serve_forever() 
