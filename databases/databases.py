from wsgiref.simple_server import make_server
from pyramid.config import Configurator
from pyramid.response import Response
from pyramid.view import view_config
import pytz
import datetime
from sqlalchemy import create_engine, MetaData, Table, Column, Integer, String, update, select
from sqlalchemy.sql import and_

engine = create_engine('sqlite:///clicks.db', echo=True)
meta = MetaData()
meta.bind = engine
time_zone_table = Table('time_zone', meta, autoload=True)

def get_time_in_timezone(timezone_tuple):
    target_zone = '/'.join([x.title() for x in timezone_tuple])
    time_zone = pytz.timezone(target_zone)
    target_time = datetime.datetime.now(time_zone)
    return target_time.strftime('%d-%m-%Y %H-%M-%S')

def update_click_counter(time_zone_name):
    connection = engine.connect()
    with connection.begin():
        update_query = update(time_zone_table, time_zone_table.c.name == time_zone_name);
        clicks_query = select([time_zone_table], and_(time_zone_table.c.name == time_zone_name))
        row = connection.execute(clicks_query).fetchone()
        updated_click_count = row[2] + 1
        connection.execute(update_query, clicks=updated_click_count)
        return updated_click_count

@view_config(route_name='time_server')
def get_time(request): 
    try:
        time_in_timezone = get_time_in_timezone(request.matchdict['timezone'])
        current_clicks = update_click_counter("/".join(request.matchdict['timezone']))
        return Response(time_in_timezone + " " + str(current_clicks))
    except pytz.UnknownTimeZoneError:
        return Response('Unknown Timezone')

if __name__ == '__main__':
    config = Configurator()
    config.add_route('time_server', '/time/*timezone')
    config.scan()

    app = config.make_wsgi_app()
    server = make_server('0.0.0.0', 8080, app)
    server.serve_forever() 
