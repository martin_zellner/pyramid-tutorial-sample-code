from wsgiref.simple_server import make_server
from pyramid.config import Configurator
from pyramid.response import Response
import email.utils

def get_time(request): #6
    return Response(body = email.utils.formatdate(localtime=True))

if __name__ == '__main__':
    config = Configurator() #1
    config.add_route('time_server', '/time') #2
    config.add_view(get_time, route_name='time_server') #3

    app = config.make_wsgi_app() #4
    server = make_server('0.0.0.0', 8080, app) #5
    server.serve_forever() 
