from sqlalchemy import create_engine, MetaData, Table, Column, Integer, String, update, select
from pytz import all_timezones

engine = create_engine('sqlite:///clicks.db', echo=True)
metadata = MetaData()
metadata.bind = engine
time_zone_table = Table('time_zone', metadata, Column('id', Integer, primary_key = True), Column('name',String(50)), Column('clicks',Integer))

time_zone_table.create(engine,checkfirst=True)

for x in all_timezones:
    insert = time_zone_table.insert(values = dict(name = x, clicks = 0))
    engine.execute(insert)
      


